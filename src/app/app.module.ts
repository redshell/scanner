import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RsScannerModule } from './rs-scanner/rs-scanner.module';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    IonicModule,
    RsScannerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
