import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';

import { RsScannerDirective } from './rs-scanner.directive';
import { RsScannerComponent } from './rs-scanner.component';
import { InjectorService } from '../injector.services';

@NgModule({
  declarations: [
    RsScannerComponent,
    RsScannerDirective
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  entryComponents: [RsScannerComponent],
  providers: [InjectorService, Diagnostic],
  exports: [
    RsScannerComponent,
    RsScannerDirective
  ]
})
export class RsScannerModule { }
