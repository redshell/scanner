import {
    AfterViewInit,
    Component, ElementRef, EventEmitter, Input, OnDestroy, Output,
    ViewChild, Renderer2
} from '@angular/core';
import { Platform } from '@ionic/angular';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';

import { Subject, Subscription } from 'rxjs';
import { QRCode } from './lib/qr-decoder/qrcode';

import * as _ from 'lodash';
declare var ZXing: any;

@Component({
    selector: 'rs-scanner',
    styleUrls: ['./rs-scanner.component.scss'],
    template: `
    <ion-content>
        <ng-container [ngSwitch]="isCanvasSupported">
            <ng-container *ngSwitchDefault>
                <canvas #canvasWrapper [hidden]="canvasHidden" [width]="canvasWidth" [height]="canvasHeight"></canvas>
                <div *ngIf="canvasHidden" class="scanner" #videoWrapper [style.width]="canvasWidth" [style.height]="canvasHeight">
                  <div class="scanner-wrapper">
                    <em></em><span></span>
                  </div>
                </div>
                <ion-fab vertical="bottom" horizontal="end" slot="fixed" *ngIf="videoDevices.length > 1">
                    <ion-fab-button color="dark" (click)="chooseCam()">
                        <ion-icon name="reverse-camera"></ion-icon>
                    </ion-fab-button>
                </ion-fab>
            </ng-container>
            <ng-container *ngSwitchCase="false">
                <p>
                    You are using an <strong>outdated</strong> browser.
                    Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
                </p>
            </ng-container>
        </ng-container>
    </ion-content>`
})
export class RsScannerComponent implements OnDestroy, AfterViewInit {
    @Input() canvasWidth = 480;
    @Input() canvasHeight = 480;
    @Input() debug = false;
    @Input() stopAfterScan = true;
    @Input() updateTime = 500;
    @Input() previewTime = 500;
    @Input() format = 'qrCode';

    @Output() captured: EventEmitter<string> = new EventEmitter();
    @Output() foundCameras: EventEmitter<MediaDeviceInfo[]> = new EventEmitter();
    @Output() stop: EventEmitter<boolean> = new EventEmitter();

    @ViewChild('videoWrapper') videoWrapper: ElementRef;
    @ViewChild('canvasWrapper') canvasWrapper: ElementRef;

    @Input() chooseCamera: Subject<MediaDeviceInfo> = new Subject();
    private chooseCamera$: Subscription;
    private selectedCam: string;
    public videoDevices: MediaDeviceInfo[] = [];

    public gCtx: CanvasRenderingContext2D;
    public videoElement: HTMLVideoElement;
    public stream: MediaStream;
    public captureTimeout: any;
    public canvasHidden = true;
    get isCanvasSupported(): boolean {
        const canvas = this.renderer.createElement('canvas');
        return !!(canvas.getContext && canvas.getContext('2d'));
    }

    constructor(
        private renderer: Renderer2,
        private platform: Platform,
        private diagnostic: Diagnostic
    ) { }

    ngAfterViewInit() {
        if (this.debug) { console.log('[scanner] ViewInit, isSupported: ', this.isCanvasSupported); }
        if (this.isCanvasSupported) {
            this.gCtx = this.canvasWrapper.nativeElement.getContext('2d');
            this.gCtx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
        }
        this.chooseCamera$ = this.chooseCamera.subscribe((camera: MediaDeviceInfo) => this.useDevice(camera));
        this.getMediaDevices().then((devices) => {
            this.videoDevices = [];
            for (const device of devices) {
                if (device.kind.toString() === 'videoinput') {
                    this.videoDevices.push(device);
                }
            }

            if (this.videoDevices.length > 0) {
                this.foundCameras.emit(this.videoDevices);
            }
        });
    }

    startScanning(device: MediaDeviceInfo) {
        const _nav: any = navigator;
        navigator.getUserMedia = _nav.getUserMedia || _nav.webkitGetUserMedia || _nav.mozGetUserMedia;

        this.platform.ready().then((_platform) => {
            if (_platform === 'dom') { return this.useDevice(device); }

            this.diagnostic.isCameraAuthorized().then((authorized) => {
                if (authorized) {
                    this.useDevice(device);
                } else {
                    this.diagnostic.requestCameraAuthorization().then((status) => {
                        if (status === this.diagnostic.permissionStatus.GRANTED) {
                            this.useDevice(device);
                        } else {
                            alert('Cannot access camera');
                        }
                    });
                }
            });
        });

        this.platform.backButton.subscribe(() => this.stop.emit(true));
    }

    stopScanning() {
        if (this.captureTimeout) {
            clearTimeout(this.captureTimeout);
            this.captureTimeout = 0;
        }
        this.canvasHidden = false;

        const stream = this.stream && this.stream.getTracks().length && this.stream;
        if (stream) {
            stream.getTracks().forEach(track => track.enabled && track.stop());
            this.stream = null;
        }
    }

    chooseCam() {
        if (this.videoDevices.length < 1) { return; }
        const idx = _.findIndex(this.videoDevices, ['deviceId', this.selectedCam]);
        const count = this.videoDevices.length;

        this.chooseCamera.next(this.videoDevices[(idx + 1) % count]);
    }

    getMediaDevices(): Promise<MediaDeviceInfo[]> {
        if (!navigator.mediaDevices || !navigator.mediaDevices.enumerateDevices) { return Promise.resolve([]); }
        return navigator.mediaDevices.enumerateDevices()
            .then((devices: MediaDeviceInfo[]) => devices)
            .catch((error: any): any[] => {
                if (this.debug) { console.warn('Error', error); }
                return [];
            });
    }

    private ScannerDecodeCallback(decoded: string) {
        if (this.stopAfterScan) {
            this.stopScanning();
            this.captured.next(decoded);
            setTimeout(() => this.stop.emit(true), this.previewTime);
        } else {
            this.captured.next(decoded);
            this.captureTimeout = setTimeout(() => this.captureToCanvas(), this.updateTime);
        }
    }

    private decode() {
        switch (this.format) {
            case 'qrCode':
                const qrCode = new QRCode();
                if (this.debug) { qrCode.debug = true; }
                qrCode.myCallback = (decoded: string) => this.ScannerDecodeCallback(decoded);

                this.gCtx.drawImage(this.videoElement, 0, 0, this.canvasWidth, this.canvasHeight);
                qrCode.decode(this.canvasWrapper.nativeElement);

                break;
            case 'pdf417':
                const imag = new Image();
                imag.src = this.canvasWrapper.nativeElement.toDataURL();
                this.gCtx.drawImage(this.videoElement, 0, 0, imag.naturalWidth, imag.naturalHeight);

                const source = new ZXing.BitmapLuminanceSource(this.gCtx, imag);
                const binarizer = new ZXing.Common.HybridBinarizer(source);
                const bitmap = new ZXing.BinaryBitmap(binarizer);
                const result = ZXing.PDF417.PDF417Reader.decode(bitmap, null, false);

                if (result.length) {
                    this.ScannerDecodeCallback(result[0].Text);
                } else {
                    throw new Error('not found');
                }

                break;

            default:
                if (this.debug) { console.log('[scanner] Invalid Format: ', this.format); }
                break;
        }
    }

    private captureToCanvas() {
        try {
            this.decode();
        } catch (e) {
            if (this.debug) { console.log('[scanner] Thrown', e); }
            if (!this.stream) { return; }
            this.captureTimeout = setTimeout(() => this.captureToCanvas(), this.updateTime);
        }
    }

    private setStream(stream: any) {
        this.canvasHidden = true;
        this.gCtx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
        this.stream = stream;
        this.videoElement.srcObject = stream;
        this.captureTimeout = setTimeout(() => {
            this.videoElement.removeAttribute('hidden');
            this.captureToCanvas();
        }, this.updateTime);
    }

    private useDevice(_device: MediaDeviceInfo) {
        const _navigator: any = navigator;
        this.selectedCam = _device.deviceId;

        if (this.captureTimeout) {
            clearTimeout(this.captureTimeout);
            this.captureTimeout = 0;

            const stream = this.stream && this.stream.getTracks().length && this.stream;
            if (stream) {
                stream.getTracks().forEach(track => track.enabled && track.stop());
                this.stream = null;
            }
        }

        if (!this.videoElement) {
            this.videoElement = this.renderer.createElement('video');
            this.videoElement.setAttribute('autoplay', 'true');
            this.videoElement.setAttribute('muted', 'true');
            this.videoElement.setAttribute('hidden', 'true');
            this.renderer.appendChild(this.videoWrapper.nativeElement, this.videoElement);
        }

        let constraints: MediaStreamConstraints;
        if (_device) {
            constraints = { audio: false, video: { deviceId: _device.deviceId } };
        } else {
            constraints = { audio: false, video: true };
        }
        _navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
            this.setStream(stream);
        }).catch((err) => {
            return this.debug && console.warn('Error', err);
        });
    }

    ngOnDestroy() {
        this.chooseCamera$.unsubscribe();
        this.stopScanning();
    }
}
