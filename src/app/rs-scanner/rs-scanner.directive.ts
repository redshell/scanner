import { Directive, ComponentRef, Input, HostListener, OnDestroy, Output, EventEmitter } from '@angular/core';
import { InjectorService } from '../injector.services';

import { RsScannerComponent as RsScannerComponent } from './rs-scanner.component';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Directive({
    selector: '[rsScanner]',
    exportAs: 'scanner'
})
export class RsScannerDirective implements OnDestroy {
    protected onDestroy = new Subject();

    private scannerRef: ComponentRef<RsScannerComponent>;
    public scanner: RsScannerComponent;

    @Input('rsScanner') config: RsScannerComponent;
    @Output() text = new EventEmitter<string>();

    constructor(
        private inject: InjectorService
    ) { }

    @HostListener('click', ['$event']) onclick() {
        this.scannerRef = this.inject.createComponent<RsScannerComponent>(RsScannerComponent, this.config);
        this.scanner = this.scannerRef.instance;

        this.scanner.foundCameras
            .pipe(takeUntil(this.onDestroy))
            .subscribe((videoDevices: MediaDeviceInfo[]) => {
                let choosenDev: MediaDeviceInfo;
                for (const dev of videoDevices) {
                    if (dev.label.includes('back')) {
                        choosenDev = dev;
                        break;
                    }
                }

                if (choosenDev) {
                    this.scanner.startScanning(choosenDev);
                } else {
                    this.scanner.startScanning(videoDevices[1] || { video: { facingMode: { exact: 'environment' } } } as any);
                }
            });

        this.scanner.captured
            .pipe(takeUntil(this.onDestroy))
            .subscribe((result: string) => this.text.emit(result));

        this.scanner.stop
            .pipe(takeUntil(this.onDestroy))
            .subscribe(() => this.ngOnDestroy());
    }

    @HostListener('document:keydown.escape', ['$event']) onScape() {
        this.ngOnDestroy();
    }

    ngOnDestroy() {
        if (!this.scanner) { return; }
        this.inject.destroyComponent(this.scannerRef);
        this.scannerRef = null;
        this.scanner = null;
        this.onDestroy.next(true);
        this.onDestroy.complete();
    }
}
