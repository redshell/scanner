import {
    Injectable,
    Injector,
    ComponentFactoryResolver,
    EmbeddedViewRef,
    ApplicationRef,
    ComponentRef
  } from '@angular/core';
  import * as _ from 'lodash';

  @Injectable()
  export class InjectorService {
    constructor(
      private componentFactoryResolver: ComponentFactoryResolver,
      private appRef: ApplicationRef,
      private injector: Injector
    ) { }

    createComponent<T>(component, config?: T): ComponentRef<T> {
      // Create a component reference from the component
      const compRef: ComponentRef<T> = this.componentFactoryResolver
        .resolveComponentFactory<T>(component)
        .create(this.injector);

      // Attach component to the appRef so that it's inside the ng component tree
      this.appRef.attachView(compRef.hostView);

      // Get DOM element from component
      const domElem = (compRef.hostView as EmbeddedViewRef<any>)
        .rootNodes[0] as HTMLElement;

      // Append DOM element to the body
      document.body.appendChild(domElem);
      _.merge(compRef.instance, config);

      return compRef;
    }

    destroyComponent(compRef: ComponentRef<any>) {
      // Wait some time and remove it from the component tree and from the DOM
      this.appRef.detachView(compRef.hostView);
      compRef.destroy();
    }
  }
